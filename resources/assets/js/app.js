
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue'); 
window.axios = require('axios'); 
window.toastr = require('toastr');
window.moment = require('moment');
window.dt = require( 'datatables.net' );
window.buttons = require( 'datatables.net-buttons' );
window.swal = require('sweetalert2');

/** 
 *  COMPONENTS
 */ 

    /**
     *  Quote Components
     */

    Vue.component('quote-form', require('./components/quotes/QuoteForm.vue'));

    /**
     *  Requirement Components
     */

    Vue.component('requirement-details-card', require('./components/requirements/RequirementDetailsCard.vue'));
    Vue.component('requirement-quote-status-box', require('./components/requirements/RequirementQuoteStatusBox.vue'));

// window.parsley = require('parsleyjs');

// window.swal = window.Vue = require('sweetalert2');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));

// const app = new Vue({
//     el: '#app'
// });