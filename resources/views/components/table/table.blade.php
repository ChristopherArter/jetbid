<h3>{{ $table['title'] or "Table" }}</h3>
        
            @if(! empty( $table['subTitle']) )
            <p><h5>{{ $table['subTitle'] }}</h5></p>
            @endif
        
<div class="card card-default">

        <div class="card-body">
        <!-- START table-responsive-->
        <div class="table-responsive">
           <table class="table table-bordered table-hover" id="{{ $table['ID'] or '' }}">
              <thead>
                 <tr>
                     @foreach( $headers as $header )
                     <th>{{ $header }}</th>
                     @endforeach
                 </tr>
              </thead>
              <tbody>
                  @foreach( $rows as $row )
                
                  @include('components.table.table-loop-item', [ 'items' => $row ])
                  @endforeach
              </tbody>
           </table>
        </div>
    </div>
        <!-- END table-responsive-->
        <div class="card-footer">
           <div class="d-flex">
              <div>
                 <div class="input-group">
                    <input class="form-control" type="text" placeholder="Search">
                    <div class="input-group-append">
                       <button class="btn btn-secondary" type="button">Button</button>
                    </div>
                 </div>
              </div>
              <div class="ml-auto">
                 <div class="input-group float-right">
                    <select class="custom-select" id="inputGroupSelect01">
                       <option value="0">Bulk action</option>
                       <option value="1">Delete</option>
                       <option value="2">Clone</option>
                       <option value="3">Export</option>
                    </select>
                    <div class="input-group-append">
                       <button class="btn btn-secondary" type="button">Apply</button>
                    </div>
                 </div>
              </div>
           </div>
        </div>
     </div>