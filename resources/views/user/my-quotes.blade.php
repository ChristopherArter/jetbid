@extends('dashboard')
@section('dashboard-content')

<div class="card card-default" id="quote_dashboard">
    <div class="card-header">
        <h3 class="card-text"> Dashboard</h3></div>
    <div class="card-body">
        <div role="tabpanel">
            <!-- Nav tabs-->
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item" role="presentation"><a class="nav-link active show" href="#home" aria-controls="home" role="tab" data-toggle="tab" aria-selected="true"><i class="fas fa-tags"></i> Submitted</a>
                </li>
                <li class="nav-item" role="presentation"><a class="nav-link" href="#profile" aria-controls="profile" role="tab" data-toggle="tab" aria-selected="false"><i class="fas fa-plane"></i> Accepted</a>
                </li>
                <li class="nav-item" role="presentation"><a class="nav-link" href="#messages" aria-controls="messages" role="tab" data-toggle="tab" aria-selected="false"><i class="far fa-user-circle"></i> Closed</a>
                </li>
                <li class="nav-item" role="presentation"><a class="nav-link" href="#settings" aria-controls="settings" role="tab" data-toggle="tab" aria-selected="false"><i class="far fa-credit-card"></i> Export</a>
                </li>
            </ul>
            <!-- Tab panes-->
            <div class="tab-content">
                <div class="tab-pane active show" id="home" role="tabpanel">
                    <table class="display table">
                            <thead>
                                <tr>
                                    <th>Quoted</th>
                                    <th>Price</th>
                                    <th>Lead Time</th>
                                    <th>Condition</th>
                                    <th>Transaction</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                         
                                <tr v-for="quote in openQuotes">
                                  
                                    <td>@{{ quote.created_at }}</td>
                                    <td>@{{ quote.price }}</td>
                                    <td>@{{ quote.lead_time }}</td>
                                    <td>@{{ quote.condition_id }}</td>
                                    <td>@{{ quote.requirement_type_id }}</td>
                                    <td><a href="#" class="btn btn-small btn-secondary">Actions</a></td>
                                </tr>

                            </tbody>
                        </table>
                </div>
                <div class="tab-pane" id="profile" role="tabpanel">Integer lobortis commodo auctor.</div>
                <div class="tab-pane" id="messages" role="tabpanel">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
                <div class="tab-pane" id="settings" role="tabpanel">
asfg
                </div>
            </div>
        </div>
    </div>
</div>
<script>
new Vue({
    el: '#quote_dashboard',
    data: {
      openQuotes: {!! json_encode($user->quotes) !!},
      closedQuotes: {!! json_encode($user->quotes->where('status', '!=', 'open' )) !!}
    },
    mounted() {

      },

    methods: {
        
    }
});
</script>
@endsection