@extends('dashboard')

@section('dashboard-content')
<?php

$user->setUserOptions(config('users.defaultOptions')); ?>

<div id="user_dashboard" class="card card-default" id="cardDemo14">
      <pre>@{{ options }}</pre>
    <div class="card-header">
        <h3 class="card-text"> Dashboard</h3></div>
    <div class="card-body">
        <div role="tabpanel">
            <!-- Nav tabs-->
            <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item" role="presentation"><a class="nav-link active show" href="#profile" aria-controls="profile" role="tab" data-toggle="tab" aria-selected="true">
                <i class="fas fa-user-circle"></i> Profile</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link" href="#terms" aria-controls="terms" role="tab" data-toggle="tab" aria-selected="false">
                    <i class="far fa-file-alt"></i>

                    Transaction Terms</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link" href="#contacts" aria-controls="contacts" role="tab" data-toggle="tab" aria-selected="false">
                <i class="far fa-user-circle"></i> Contacts </a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link" href="#subscription" aria-controls="subscription" role="tab" data-toggle="tab" aria-selected="false">
                <i class="far fa-credit-card"></i> Subscription</a>
        </li>
            </ul>
            <!-- Tab panes-->
            <div class="tab-content">
                <div class="tab-pane active show" id="profile" role="tabpanel">Suspendisse velit erat, vulputate sit amet feugiat a, lobortis nec felis.</div>
                <div class="tab-pane" id="terms" role="tabpanel">

                    <div v-for="(key, value) in options.terms">
                        <div class="form-group">
                            <label> @{{ value }}</label>
                            <textarea rows="8" class="form-control">@{{ key  }}</textarea>
                        </div>

                         

                    </div>


                </div>
                <div class="tab-pane" id="contacts" role="tabpanel">

                <div v-for="contact in options.contacts">
                   <h5><span class="badge badge-success">@{{ contact.position }}</span></h5>
                   <div class="form-group">
                    <label>Name</label>
                    <input class="form-control" v-model="contact.name">
                   </div>

                   <div class="form-group">
                   <label>Email</label>
                    <input class="form-control" v-model="contact.email">
                   </div>

                    <div class="form-group">
                    <label>Position</label>
                    <input class="form-control" v-model="contact.position">
                    </div>
                    <hr>
                </div>
                    <ul>
                  
                    </ul>

                </div>
                <div class="tab-pane" id="subscription" role="tabpanel">
asfg
                </div>
            </div>
        </div>
    </div>
</div>
<script>
        var userDash = new Vue({
            el: '#user_dashboard',
            data: {
               options: {!! $user->options !!}
            },
            mounted() {
               console.log(this.options.contacts);
            },
            methods: {

            }
        });

</script>
@endsection