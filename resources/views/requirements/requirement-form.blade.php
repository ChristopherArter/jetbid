<?php
    if ( Route::currentRouteName() == 'requirement.create' )
    { 
        $action = route('requirement.store');
    } else if ( Route::currentRouteName() == 'requirement.edit' ) {
        $action = route('requirement.update')
    }
?>

<form action={{ $action }} method="POST">
        @csrf
        @if($action == route('requirement.edit') )
        @method('PUT')
        @endif
        <div class="col-md-10">
                <input class="form-control" type="text" value="{{ $requirement->part_number or '' }}">
                <span class="form-text">A block of help text that breaks onto a new line and may extend beyond one line.</span>
        </div>
        <button class="btn btn-primary" type="submit">Submit form</button>
    </form>