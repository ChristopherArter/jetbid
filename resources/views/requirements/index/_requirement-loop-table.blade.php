<div class="card">
        <div class="card-header">
           <div class="card-title">Zero configuration</div>
           <div class="text-sm">DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: $().DataTable();.</div>
        </div>
        <div class="card-body">
           <div id="datatable1_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer"><div class="row"><div class="col-sm-12 col-md-6"><div class="dataTables_length" id="datatable1_length"><label><select name="datatable1_length" aria-controls="datatable1" class="form-control form-control-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> records per page</label></div></div><div class="col-sm-12 col-md-6"><div id="datatable1_filter" class="dataTables_filter"><label><em class="ion-search"></em><input type="search" class="form-control form-control-sm" placeholder="" aria-controls="datatable1"></label></div></div></div><div class="row"><div class="col-sm-12"><table class="table table-striped my-4 w-100 dataTable no-footer dtr-inline" id="datatable1" role="grid" aria-describedby="datatable1_info" style="width: 1529px;">
              <thead>
                 <tr role="row"><th data-priority="1" class="sorting" tabindex="0" aria-controls="datatable1" rowspan="1" colspan="1" style="width: 213px;" aria-label="Engine: activate to sort column ascending">Engine</th><th class="sorting" tabindex="0" aria-controls="datatable1" rowspan="1" colspan="1" style="width: 368px;" aria-label="Browser: activate to sort column ascending">Browser</th><th class="sorting" tabindex="0" aria-controls="datatable1" rowspan="1" colspan="1" style="width: 328px;" aria-label="Platform(s): activate to sort column ascending">Platform(s)</th><th class="sort-numeric sorting_desc" tabindex="0" aria-controls="datatable1" rowspan="1" colspan="1" style="width: 247px;" aria-label="Engine version: activate to sort column ascending" aria-sort="descending">Engine version</th><th class="sort-alpha sorting" data-priority="2" tabindex="0" aria-controls="datatable1" rowspan="1" colspan="1" style="width: 183px;" aria-label="CSS grade: activate to sort column ascending">CSS grade</th></tr>
              </thead>
              <tbody>
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
                 
              <tr class="gradeA odd" role="row">
                    <td class="" tabindex="0">Webkit</td>
                    <td>Safari 3.0</td>
                    <td>OSX.4+</td>
                    <td class="sorting_1">522.1</td>
                    <td>A</td>
                 </tr><tr class="gradeA even" role="row">
                    <td class="" tabindex="0">Webkit</td>
                    <td>iPod Touch / iPhone</td>
                    <td>iPod</td>
                    <td class="sorting_1">420.1</td>
                    <td>A</td>
                 </tr><tr class="gradeA odd" role="row">
                    <td class="" tabindex="0">Webkit</td>
                    <td>OmniWeb 5.5</td>
                    <td>OSX.4+</td>
                    <td class="sorting_1">420</td>
                    <td>A</td>
                 </tr><tr class="gradeA even" role="row">
                    <td class="" tabindex="0">Webkit</td>
                    <td>Safari 2.0</td>
                    <td>OSX.4+</td>
                    <td class="sorting_1">419.3</td>
                    <td>A</td>
                 </tr><tr class="gradeA odd" role="row">
                    <td class="" tabindex="0">Webkit</td>
                    <td>S60</td>
                    <td>S60</td>
                    <td class="sorting_1">413</td>
                    <td>A</td>
                 </tr><tr class="gradeA even" role="row">
                    <td class="" tabindex="0">Webkit</td>
                    <td>Safari 1.3</td>
                    <td>OSX.3</td>
                    <td class="sorting_1">312.8</td>
                    <td>A</td>
                 </tr><tr class="gradeA odd" role="row">
                    <td class="" tabindex="0">Webkit</td>
                    <td>Safari 1.2</td>
                    <td>OSX.3</td>
                    <td class="sorting_1">125.5</td>
                    <td>A</td>
                 </tr><tr class="gradeA even" role="row">
                    <td class="" tabindex="0">Presto</td>
                    <td>Nintendo DS browser</td>
                    <td>Nintendo DS</td>
                    <td class="sorting_1">8.5</td>
                    <td>C/A<sup>1</sup>
                    </td>
                 </tr><tr class="gradeA odd" role="row">
                    <td tabindex="0" class="">Trident</td>
                    <td>Internet Explorer 7</td>
                    <td>Win XP SP2+</td>
                    <td class="sorting_1">7</td>
                    <td>A</td>
                 </tr><tr class="gradeA even" role="row">
                    <td tabindex="0" class="">Trident</td>
                    <td>Internet Explorer 6</td>
                    <td>Win 98+</td>
                    <td class="sorting_1">6</td>
                    <td>A</td>
                 </tr></tbody>
           </table></div></div><div class="row"><div class="col-sm-12 col-md-5"><div class="dataTables_info" id="datatable1_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div></div><div class="col-sm-12 col-md-7"><div class="dataTables_paginate paging_simple_numbers" id="datatable1_paginate"><ul class="pagination"><li class="paginate_button page-item previous disabled" id="datatable1_previous"><a href="#" aria-controls="datatable1" data-dt-idx="0" tabindex="0" class="page-link"><em class="fa fa-caret-left"></em></a></li><li class="paginate_button page-item active"><a href="#" aria-controls="datatable1" data-dt-idx="1" tabindex="0" class="page-link">1</a></li><li class="paginate_button page-item "><a href="#" aria-controls="datatable1" data-dt-idx="2" tabindex="0" class="page-link">2</a></li><li class="paginate_button page-item "><a href="#" aria-controls="datatable1" data-dt-idx="3" tabindex="0" class="page-link">3</a></li><li class="paginate_button page-item "><a href="#" aria-controls="datatable1" data-dt-idx="4" tabindex="0" class="page-link">4</a></li><li class="paginate_button page-item "><a href="#" aria-controls="datatable1" data-dt-idx="5" tabindex="0" class="page-link">5</a></li><li class="paginate_button page-item "><a href="#" aria-controls="datatable1" data-dt-idx="6" tabindex="0" class="page-link">6</a></li><li class="paginate_button page-item next" id="datatable1_next"><a href="#" aria-controls="datatable1" data-dt-idx="7" tabindex="0" class="page-link"><em class="fa fa-caret-right"></em></a></li></ul></div></div></div></div>
        </div>
     </div>