@extends('dashboard')

@section('dashboard-content')

<div id="requirement_table" class="card">
    <div class="card-header">
       <h3>{{ $pagetitle or "Open" }} Requirements</h3>
    </div>
    <div class="card-body">
    
        <table class="table table-responsive-sm">
            <thead>
               <tr>
                  <th>Part Number</th>
                  <th>Description</th>
                  <th>Status</th>
                  <th>Application</th>
                  <th>Actions</th>
               </tr>
            </thead>
            <tbody>
               <tr v-for="req in requirements">
                  <td>@{{ req.part_number }}</td>
                  <td>@{{ req.description }}</td>
                  <td><span class="badge badge-primary">@{{ req.status }}</span></td>
                  <td>@{{ req.aircraft.application }} </td>
                  <td>
                    <a v-bind:href="base_url + '/quote/' + req.id + '/new'" class="btn btn-primary">Quote</a>
                  </td>
               </tr>
            </tbody>
         </table>
       
    </div>
 </div>
@endsection

@section('dashboard-right-sidebar')

@include('components.stats._statsbox', ['stat' => $requirements->count(), 'title' => 'Open Requirements', 'icon'=>'fas fa-plane'])
<div class="card card-default">
    <div class="card-header">
       <div class="card-title">Latest activities</div>
    </div>
    <!-- START list group-->
    <div class="list-group">
       <!-- START list group item-->
       <div class="list-group-item">
          <div class="media">
             <div class="align-self-start mr-2">
                <span class="fa-stack">
                   <em class="fa fa-circle fa-stack-2x text-purple"></em>
                   <em class="fa fa-cloud-upload fa-stack-1x fa-inverse text-white"></em>
                </span>
             </div>
             <div class="media-body text-truncate">
                <p class="mb-1"><a class="text-purple m-0" href="#">NEW FILE</a>
                </p>
                <p class="m-0">
                   <small><a href="#">Bootstrap.xls</a>
                   </small>
                </p>
             </div>
             <div class="ml-auto">
                <small class="text-muted ml-2">15m</small>
             </div>
          </div>
       </div>
       <!-- END list group item-->
       <!-- START list group item-->
       <div class="list-group-item">
          <div class="media">
             <div class="align-self-start mr-2">
                <span class="fa-stack">
                   <em class="fa fa-circle fa-stack-2x text-info"></em>
                   <em class="fa fa-file-text-o fa-stack-1x fa-inverse text-white"></em>
                </span>
             </div>
             <div class="media-body text-truncate">
                <p class="mb-1"><a class="text-info m-0" href="#">NEW DOCUMENT</a>
                </p>
                <p class="m-0">
                   <small><a href="#">Bootstrap.doc</a>
                   </small>
                </p>
             </div>
             <div class="ml-auto">
                <small class="text-muted ml-2">2h</small>
             </div>
          </div>
       </div>
       <!-- END list group item-->
       <!-- START list group item-->
       <div class="list-group-item">
          <div class="media">
             <div class="align-self-start mr-2">
                <span class="fa-stack">
                   <em class="fa fa-circle fa-stack-2x text-danger"></em>
                   <em class="fa fa-exclamation fa-stack-1x fa-inverse text-white"></em>
                </span>
             </div>
             <div class="media-body text-truncate">
                <p class="mb-1"><a class="text-danger m-0" href="#">BROADCAST</a>
                </p>
                <p class="m-0"><a href="#">Read</a>
                </p>
             </div>
             <div class="ml-auto">
                <small class="text-muted ml-2">5h</small>
             </div>
          </div>
       </div>
       <!-- END list group item-->
       <!-- START list group item-->
       <div class="list-group-item">
          <div class="media">
             <div class="align-self-start mr-2">
                <span class="fa-stack">
                   <em class="fa fa-circle fa-stack-2x text-success"></em>
                   <em class="fa fa-clock-o fa-stack-1x fa-inverse text-white"></em>
                </span>
             </div>
             <div class="media-body text-truncate">
                <p class="mb-1"><a class="text-success m-0" href="#">NEW MEETING</a>
                </p>
                <p class="m-0">
                   <small>On
                      <em>10/12/2015 09:00 am</em>
                   </small>
                </p>
             </div>
             <div class="ml-auto">
                <small class="text-muted ml-2">15h</small>
             </div>
          </div>
       </div>
       <!-- END list group item-->
    </div>
    <!-- END list group-->
    <!-- START card footer-->
    <div class="card-footer"><a class="text-sm" href="#">Load more</a>
    </div>
    <!-- END card-footer-->
 </div>
@endsection

@section('footer-scripts')
<script>

    new Vue({
        el: '#requirement_table',
        data: {
          requirements: [],
          loading:false,
          base_url: '{!! url('/') !!}'
        },
        mounted() {
            this.getRequirements();
          },
          methods: {
            
            getRequirements: function(){ 
                this.loading = true;
                axios.get("{!! url('requirements/json') !!}",{ headers: 
                { 'X-CSRF-TOKEN': '{!! csrf_token() !!}', }
        }).then( response => {
              this.loading = false;
               this.requirements = response.data
            });
          }
        }
        });
    </script>
    @endsection