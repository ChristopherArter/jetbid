@extends('dashboard')

@section('dashboard-content')
<?php
    if ( Route::currentRouteName() == 'requirement.create' )
    { 
        $action = route('requirement.store');

    } else if ( Route::currentRouteName() == 'requirement.edit' ) {
        $action = route('requirement.update', $requirement);
    }
?>

<form v-on:submit.prevent="onSubmit" action={{ $action }} method="POST" id="requirement_form">
        @csrf
        <?php 

        // spoof route if it's an edit route.
        if ( Route::currentRouteName() == 'requirement.edit') { ?>
            @method('PUT')
        <?php } ?>

                
        <div class="form-group row">
                <label class="col-md-2 col-form-label"><strong>Aircraft</strong></label>
                <div class="col-md-10">

                        <select class="custom-select custom-select mb-3" v-model="requirement.aircraft_id">
                                <option v-for="aircraft in aircrafts" v-bind:value="aircraft.id">
                                  @{{ aircraft.tail_number }}
                                </option>
                              </select>
                </div>
             </div>

        <div class="form-group row">
            <label class="col-md-2 col-form-label"><strong>Part Number</strong></label>
            <div class="col-md-10">
                
                    <input name="part_number" class="form-control" type="text" v-model="requirement.part_number">
            </div>
        </div>
        
        <div class="form-group row">
                <label class="col-md-2 col-form-label"><strong>Description</strong></label>
                <div class="col-md-10">
                        <input name="description" class="form-control" type="text"  v-model="requirement.description">
                </div>
        </div>

        <div class="form-group row">
                <label class="col-md-2 col-form-label"><strong>Details</strong></label>
                <div class="col-md-10">
                        <textarea name="details" class="form-control" aria-label="With textarea" v-model="requirement.details">@{{ requirement.details }}</textarea>
                        <span class="form-text">Details of the requirement. Condition, sale type, terms, etc.</span>
                </div>
        </div>

        <div class="form-group row">
                <label class="col-md-2 col-form-label"><strong>Location</strong></label>
                <div class="col-md-10">
                        <input name="location" class="form-control" type="text" v-model="requirement.location">
                        <span class="form-text">Where will this need to be shipped? Airport, address, MRO facility, etc.</span>
                </div>
        </div>
<hr>
        <fieldset>

              
                <div class="form-group row">
                   <label class="col-md-2 col-form-label"><strong>Transaction Type</strong></label>
                   <div class="col-md-6">

                       <div v-for="type in types">
                        <div class="form-check form-check-inline">
                                
                                <label v-bind:for="type.id" class="switch switch-lg">                                   
                                    <input v-bind:id="type.id" @click="typeClick" class="form-check-input" type="checkbox" v-bind:checked="typeIsChecked(type)" v-bind:value="type.id" >
                                    <span></span>
                                    </label>
                                    <p>
                                    <label class="form-check-label" for="defaultCheck1"> <p style="margin-left:15px !important;">@{{ type.name}}<p></label></p>
                        </div>
                    </div>
                   </div>
                </div>
 
             </fieldset>

        <button id="submit" v-on:click="submit" class="btn btn-primary btn-lg" type="submit"><i class="fas fa-cloud"></i> Save</button>
    </form>

    <script>

            new Vue({
                el: '#jetbid_app',
                data: {
                  requirement: {!! json_encode($requirement) !!},
                  aircrafts: {!! json_encode($aircrafts) !!},
                  loading: false,
                  types: [],
                  checked: true
                },
                mounted() {
                    
                    this.requirement.types = {!! $activeTypes !!};

                    axios.get("{!! route('requirementtype.index') !!}",{ headers: 
                        { 'X-CSRF-TOKEN': '{!! csrf_token() !!}', }
                }).then( response => {
                    
                      this.types = response.data
                    });
                  },

                methods: {

                    typeIsChecked: function(type){

                        for ( var reqType in this.types ){
                            console.log(reqType);
                            if (reqType == type.id){
                               
                                return true;
                            }
                        }
                        return false;
                    },
                    typeClick: function(event){
                        console.log('yep');
                       
                    },
                    submit: function(event) {
                        this.loading = true;
                        if('{!! Route::currentRouteName() !!}' == 'requirement.edit'){
                            this.requirement._method = 'PUT';
                        }
                        axios.post('{!! $action !!}', this.requirement, 
                        { headers: 
                            { 'X-CSRF-TOKEN': '{!! csrf_token() !!}', }
                    }).then( function(response){
                        this.loading = false;
                        this.requirement = response;
                        swal("Saved", "", "success")
                
                    }).catch(error => {
                        
                        if (error ) {
                            console.log(error);
                          swal("Oh no!", error.message, "error");
                        } else {
                          swal.stopLoading();
                          swal.close();
                        }
                      });
                  }
                }
              })
            </script>
@endsection