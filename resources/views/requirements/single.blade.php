@extends('dashboard')

@section('dashboard-content')

<h2>Requirement {{ $requirement->id }} @if($requirement->aog)<span class="badge badge-danger">AOG</span>@endif </h2>
<div class="row">
<div class="col-xl-4">
       
    {{--  PART NUMBER BOX  --}}
        <div class="card">
           <div class="card-body">
                <p class="text-muted">Part Number</p>
              <h3 class="mt-0">{{ $requirement->part_number}}</h3>
             

           </div>
        </div>
     </div>

     {{--  DESCRIPTION  --}}
     <div class="col-xl-4">
            <div class="card">
                    <div class="card-body">
                         <p class="text-muted">Description</p>
                       <h3 class="mt-0">{{ $requirement->description }}</h3>
                      
         
                    </div>
                 </div>
              </div>




     {{--  APPLICATION --}}
     <div class="col-xl-4">
            <div class="card">
                    <div class="card-body">
                         <p class="text-muted">Application</p>
                       <h3 class="mt-0">{{ $requirement->aircraft->application }}</h3>
                      
         
                    </div>
                 </div>
              </div>



    </div>
    <div class="row">
             {{--  DETAILS  --}}
     <div class="col-xl-12">
             <label>Details</label>
            <div class="card">
                    <div class="card-body">
                         <p class="text-muted">Details</p>
                       {{ $requirement->details }}
                      
         
                    </div>
                 </div>
              </div>
            </div>

            <hr>


            <div class="row" id="quote_form">
                        <div class="col-xl-12">
                                        <legend>Quote this Requirement  <i v-if="loading" class="fas fa-sync-alt fa-spin"></i></legend>
                                </div>

                    <div class="col-xl-12">

                            <div v-for="type in typesWithQuotes" class="card card-default">
                                        <div v-bind:class="{ ' text-white bg-success': type.quote.created_at }" class="card-header">
                                        
                                        
                                               <strong> @{{ type.name }}</strong>
                                     
                                        <span  v-if="type.quote.created_at" > - <i class="far fa-check-circle"></i> Quoted on @{{ type.quote.created_at}}</span>

                                        <a @click="saveQuote(type.quote)" class="float-right" href="#" style="color:#fff;">
                                                        <i class="fas fa-save"></i> save 


                                        </a>

                                        </div>
                                    <div class="card-body">
                                       <div class="form-group row">

                                             
                                            <div class="col-md-12">
                                                    
                                                        <label>Price (USD)  </label>
                                                        <div class="input-group mb-3">
                                                               
                                                                        <div class="input-group-prepend">
                                                                           <span class="input-group-text" id="basic-addon1">$</span>
                                                                        </div>
                                                                        
                                                                        <input v-model="type.quote.price" data-parsley-pattern="^[0-9]*\.[0-9]{2}$" autocomplete="off" name="part_number" class="form-control" type="text">
                                                          
                                                        </div>

                                                        <label>Lead time <small>(Optional)</small></label>
                                                        <div class="input-group mb-3">
                                                                        
                                                                        <input v-model="type.quote.lead_time" autocomplete="off" name="part_number" class="form-control" type="text">
                                                           
                                                        </div>

                                                        <label>Details</label>
                                                        <textarea rows="4" name="details" class="form-control" aria-label="With textarea" >@{{ type.quote.details }}</textarea>
                                                   
                                            </div>
                                       
                                        </div>
                                    </div>
                                  </div>
                        <div class="clearfix">
                                        <div class="float-right">
                                                <button @click="sync(typesWithQuotes)" class="btn btn-primary btn-oval btn-lg" type="submit">
                                                        <i class="fas fa-paper-plane fa-lg" ></i>
                                                
                                                <h4 style="line-height: 0px; display:inline-block; margin:10px; padding:10px;">Send Quote</h4>
                                                </button>
                                        </div>
                                        </div>
                </div>
        </div>

       

        

                <script>

                        var form = new Vue({
                            el: '#quote_form',
                            data: {
                                loading:false,
                              requirement: {!! json_encode($requirement) !!},
                              types:  [],
                              baseurl: '{!! url('/') !!}',
                              hasQuoted: {!! json_encode($hasQuoted) !!},
                              quotes: {!! json_encode($quotes) !!},
                              emtpyQuote: {
                                price:"",
                                description: "",
                                id:"",
                                lead_time:"",
                                condition_id:"" },
                        
                        
                              typesWithQuotes: []
                              
                            },
                            mounted() {
                                this.getTypes();
                                this.typesWithQuotes = this.buildQuotesFromRequirementTypes( this.types, this.quotes );
                                
                              },
            
                            methods: {

                                saveQuote: function( quote ){
                                        axios.post( this.baseurl + '/quote/sync', quote,
                                        { headers: 
                                                { 'X-CSRF-TOKEN': '{!! csrf_token() !!}',
                                                 'Content-Type':'application/json' }
                                        }).then( response => {
                                                quote = response.data;
                                                return quote;
                                        });
                                },

                                // fetch types for requirement
                                getTypes: function(){
                                        this.loading = true;
                                        axios.get('{!! route('requirement.types', $requirement->id ) !!}',{ headers: 
                                        { 'X-CSRF-TOKEN': '{!! csrf_token() !!}', }
                                }).then( response => {
                                this.loading = false;
                                       this.types = response.data
                                       this.typesWithQuotes = this.buildQuotesFromRequirementTypes( this.types, this.quotes );
                                    });
                                  },

                                // pair matching quotes with requirement types
                                buildQuotesFromRequirementTypes: function(types, quotes){

                                        var emtpyQuote = {
                                                price:"",
                                                description: "",
                                                id:"",
                                                lead_time:"",
                                                condition_id:""
                                        };

                                        types.forEach( function( type ){
                                                
                                                if(quotes.length > 0){
                                                        
                                                        quotes.forEach( function( quote ){
                                                               
                                                                if( type.id == quote.requirement_type_id ){
                                                                        type.quote = quote;
                                                                } else {
                                                                        type.quote = emtpyQuote;
                                                                }
                                                        });

                                                } else {
                                                        type.quote = emtpyQuote;
                                                }
                                        });
                                        return types;
                                },

                                
                              sync: function( types ){
                                types.forEach( function( type ){
                                        this.saveQuote(type.quote);
                                });
                              }
                            }
                          })
                        </script>
         @endsection

@section('dashboard-right-sidebar')

@endsection