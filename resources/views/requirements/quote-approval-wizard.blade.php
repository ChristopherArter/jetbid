@extends('dashboard')

@section('dashboard-content')

    <div id="wizard">

       <div class="card" v-for="quote in quotes">
        <div class="card-header bg-custom card-header-primary">
            <div class="float-right btn-group">
                    <button @click="declineQuote(quote)" class="btn btn-outline-light btn-sm">Decline</button>
                    <button @click="removeQuote(quote)" class="btn btn-outline-light btn-sm">Remove</button>
            </div>
           
        </div>
        <div class="card-body">
                <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                          <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Quote</a>
                          <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false"> <i class="fas fa-info-circle"></i> Terms</a>
                      
                        </div>
                      </nav>
                      <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                <div class="row">
                                        <dt class="col-sm-3">Part Number</dt>
                                        <dd class="col-sm-9">@{{ quote.requirement.part_number }}</dd>
                    
                                        <dt class="col-sm-3">Description</dt>
                                        <dd class="col-sm-9">@{{ quote.description }}</dd>
                    
                                        <dt v-if="quote.lead_time" class="col-sm-3">Lead Time</dt>
                                        <dd v-if="quote.lead_time" class="col-sm-9">@{{ quote.lead_time }}</dd>
                    
                                        <dt class="col-sm-3">Price</dt>
                                        <dd class="col-sm-9">@{{ quote.price }}</dd>
                    
                    
                                        <dt class="col-sm-3">Vendor</dt>
                                        <dd class="col-sm-9">
                                            
                                            <ul class="list-unstyled">
                                                <li><i class="fas fa-user-circle"></i> user here</li>
                                                <li><i class="fas fa-user-circle"></i> Flight Motion</li>
                                                <li><i class="fas fa-envelope-square"></i> <a href="mailto:">asdf</a></li>
                                            </ul>
                                        </dd>
                          
                                </div>
                        </div>
                        <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                            Terms and conditions here
                        </div>
                      </div>


<hr>
        <div class="row">

        <div class="col-sm-4">
                <div class="form-group">
                    <label>Ship to:</label>
                    <select class="form-control">
                        <option v-for="address in userOptions.shipping.addresses" v-bind:selected="address.default">@{{ address.label }}</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-4">
                    <div class="form-group">
                        <label>Shipping Carrier</label>
                        <select class="form-control">
                
                            <option v-for="carrier in userOptions.shipping.carriers" v-bind:selected="carrier.default"> @{{carrier.label}} - @{{ carrier.account_number }} - @{{ carrier.rate }}</option>
                        </select>
                    </div>
                </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label>Shipping Rate</label>
                   <input class="form-control">
                </div>
            </div>
        </div>
        <div class="row">
                <div class="col-sm-12">
                    
                        <div  v-for="term in userOptions.terms" v-if="term.type == 'purchase'" class="form-group">
                            <label>Purchase Terms</label>
                        
                        <textarea class="form-control" rows="4" >@{{ term.content }}</textarea>

                    </div>
                </div>
        </div>

        </div>
       </div>
    </div>

@endsection

@section('dashboard-right-sidebar')
<div class="card">
    <div class="card-body">
        <a href="{{ route('quotes.confirmation') }}" class="btn btn-block btn-primary btn-lg">Submit</a>
    </div>
</div>
@endsection

@section('footer-scripts')

<script>

    var wizard = new Vue({
        el: '#wizard',
        data: {
            quotes: [],
            quoteSync: false,
            userOptions: {!! $user->options !!},
            selected: [],
        },
        mounted(){
            this.getStagedQuotes();
        },
        methods: {

            getCarrierRate: function(carrier)
            {

            },

            getStagedQuotes: function(){
                var vm = this;
                axios.get('{!! route("quotes.staged") !!}', { headers:  { 'X-CSRF-TOKEN': '{!! csrf_token() !!}', }})
                .then( function(response){
                  vm.quotes = response.data;
                  vm.quoteSync = false;
              }).catch(error => {
                  if (error ) {
                      console.log(error);
                  } 
                });
              },

            removeQuote(quote)
            {
                vm = this;
                var index = vm.quotes.indexOf(quote);
                vm.quotes.splice(index, 1);
                vm.syncQuotes();
                vm.getStagedQuotes();

            },

            declineQuote(quote)
            {
                swal("Are you sure you want to decline this quote?", {
                    buttons: ["Oops!", "Yes, decline this quote."],
                  });
                
                  quote.status = 'declined';
                 quote.confirmed = true;
                  this.removeQuote(quote);

            },

                      // sync the quotes.
      syncQuotes: function(){
        var vm = this;
        axios.post('{!! route("quote.sync") !!}', vm.quotes, 
          { headers: 
            { 'X-CSRF-TOKEN': '{!! csrf_token() !!}', }
        }).then( function( response ){
          
    }).catch(error => {
        if ( error ) {
            console.log(error);
        } 
      });

    }
        }
    });

</script>
<style>
.bg-custom {
    background: #8360c3;  /* fallback for old browsers */
background: -webkit-linear-gradient(to left, #2ebf91, #8360c3);  /* Chrome 10-25, Safari 5.1-6 */
background: linear-gradient(to left, #2ebf91, #8360c3); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
color: #fff !important;

}
</style>
@endsection