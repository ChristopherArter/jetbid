@extends('dashboard')

@section('dashboard-content')

<?php $user = Auth::user(); ?>

<div id="recieved_quotes" class="card">

        <div class="card-header bg-custom">
           <div class="float-left">
              <h3>Recieved Quotes</h3> 
           </div>

           {{--  Accept all / reject all buttons  --}}
           <div class="float-right">
              <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                  <button @click="acceptAll()" type="button" class="btn btn-outline-light"><i class="fas fa-check"></i>
                    Accept All</button>
                  <button @click="resetAll()" type="button" class="btn btn-outline-light"> <i class="fas fa-undo"></i> Reset All</button>
                </div>
           </div>
        </div>

        <div class="card-body">
          
        {{--  Quotes Table  --}}
        <div class="table-responsive">
            <table id="requirement_table" class="table table-borderless">

                <thead>
                   <tr>
                      <th>Part Number</th>
                      <th>Description</th>
                      <th>Price</th>
                      <th>Date</th>
                      <th>Tail</th>
                      <th>Application</th>
                      <th>Actions</th>
                   </tr>
                </thead>
                <tbody>
                
                   <tr v-for="quote in quotes">
                     @{{ quote }}
                      <td>@{{ quote.part_number }}</td>
                      <td>Selected: @{{ quote.selected }} / Declined: @{{ quote.declined }}</td>
                      <td>@{{ quote.price }}</td>
                      <td>@{{ quote.created_at }}
                      <td>tail here</td>
                      <td>app here</td>
                      
                      {{--  QUOTE BUTTONS  --}}
                      <td>
                        <div class="btn-group" >

                          {{--  Select quote button  --}}
                          <button @click="quote.selected = true; quote.declined = false;" class="btn"
                          v-bind:class="{'btn-success': quote.selected == true, 'btn-secondary': quote.selected === null || quote.selected === false }">
                            <i class="fas fa-check"></i>
                          </button>

                          {{--  Decline button @TODO: MAKE THIS WORK  --}}
                          <button class="btn btn-secondary" @click="quote.declined = true; quote.selected = false;"
                          v-bind:class="{'btn-danger': quote.declined == true}"
                          >
                          <i class="fas fa-ban"></i></button>

                          {{--  View Quote @TODO: MAKE THIS VIEW  --}}
                          <button data-toggle="modal" v-bind:data-target="'#quote_modal'+quote.id" class="btn btn-secondary">
                              <i class="far fa-eye"></i></button>

                          {{--  UNDO   --}}
                          <button @click="quote.selected = null; quote.declined = false;" class="btn btn-secondary">
                              <i class="fas fa-undo"></i></button>

                        </div>

                        {{--  QUOTE VIEW MODAL  --}}
                        <!-- Modal -->
                        <div class="modal fade" v-bind:id="'quote_modal'+quote.id" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                @{{ quote }}
                              </div>
                              <div class="modal-footer">
                                  <button @click="quote.selected = true" data-dismiss="modal" 
                                  class="btn"
                    
                                  v-bind:class="{'btn-success': quote.selected == true, 'btn-secondary': quote.selected === null || quote.selected === false }">
        
                                  <i class="fas fa-check"></i> Accept
                                  
                                </button>
                                  <button @click="quote.selected = false" data-dismiss="modal" class="btn btn-secondary"
                                  v-bind:class="{'btn-danger': quote.selected == false, 'btn-secondary':quote.selected === null }">
                                  <i class="fas fa-ban"></i> Decline </button>
        
        
                                  <button @click="quote.selected = null" data-dismiss="modal" class="btn btn-secondary">
                                      <i class="fas fa-undo"></i></button>
                              </div>
                            </div>
                          </div>
                        </div>

                      </td>
                      {{--  END CONTROLS AND MODAL CELL  --}}

                   </tr>

                </tbody>
                <tfoot>
                    <tr>
                      <th>Part Number</th>
                      <th>Description</th>
                      <th>Price</th>
                      <th>Dat</th>
                      <th>Tail</th>
                      <th>Application</th>
                      <th>Actions</th>
                    </tr>
                </tfoot>
             </table>

             {{--  /.table-responsive  --}}
            </div>

            {{--  /.card-body  --}}
        </div>

        {{--  /#recieved_quotes  --}}
     </div>
@endsection


@section('dashboard-right-sidebar')

{{--  SIDEBAR  --}}
  <div class="card">
  <div class="card-header">
    <div class="row">

      {{--  SAVE BUTTON  --}}
        <div class="col-sm-12">
            <button class="btn btn-xl btn-block btn-primary"  v-if="hasUnsavedQuotes" @click="syncQuotes()"><i class="fas fa-save"></i> Save</button>
                <a  v-bind:class="{ 'disabled': disableNextButton}" class="btn btn-xl  btn-block btn-primary" href="{{ route('quotes.review') }}" aria-disabled="true" v-if="!hasUnsavedQuotes">Next <i class="fas fa-chevron-circle-right"></i></a>
            </div>

    </div>
  </div>
      <div v-bind:class="{ 'text-center': quoteSync}" class="card-body">

        {{--  Quote sync spinner  --}}
        <i v-if="quoteSync" class="fas fa-3x fa-spinner fa-spin"></i>
          <ul  v-if="!quoteSync" class="nav nav-tabs" id="myTab" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" id="accepted_tab" data-toggle="tab" href="#accepted" role="tab" aria-controls="accepted" aria-selected="true">
                  <span class="badge badge-success" v-if="totalAccepted > 0">@{{ totalAccepted }}</span> Accepted</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="declined_tab" data-toggle="tab" href="#declined" role="tab" aria-controls="declined" aria-selected="false">
                    <span class="badge badge-secondary" v-if="totalDeclined> 0">@{{ totalDeclined }}</span>
                  Declined</a>
              </li>

            </ul>
            <div  v-if="!quoteSync" class="tab-content" id="myTabContent">
              <div class="tab-pane fade show active" id="accepted" role="tabpanel" aria-labelledby="accepted_tab">
                <ul class="list-group">

                    <ul id="example-1">
                        <li v-for="item in stagedQuotes">
                          @{{ item.status }}
                        </li>
                      </ul>

                    </ul>
              </div>
              <div class="tab-pane fade" id="declined" role="tabpanel" aria-labelledby="declined_tab">
                  <ul class="list-group">
                  </ul>
              </div>
            </div>
      </div>
      </div>

@endsection

@section('footer-scripts')
<script>

    var recQuotes = new Vue({

        el: '#jetbid_dashboard',

        data: {
          quotes: {!! $quotes !!},
          stagedQuotes: '',
          quoteSync: false,
        },

        mounted() {

          this.getStagedQuotes();
          this.initializeDataTable();
          
        },

        created: function() {
  
          var returnArray = [];
          var vm = Vue;
          this.quotes.forEach(function(quote){
            vm.set(quote, 'selected', null);
            vm.set(quote, 'declined', null);
          });

           
        },

        computed: {

          totalAccepted: function(){
            return this.stagedQuotes.length;
          },

          totalDeclined: function(){
            declined = [];
            this.quotes.forEach( function(quote){
              if(quote.selected === false ){
                declined.push(quote);
              }
            });
            return declined.length;
          },

          hasUnsavedQuotes: function(){
            
            var approved = this.getAllApprovedQuotes();
            var staged = this.stagedQuotes;
            if( approved.length != staged.length ){
              return true;
            } else {
              return false;
            }
          },

          disableNextButton: function()
          {
            if(this.stagedQuotes.length == 0){
              return true;
            }
            return false;
          }

        },
        methods: {

  // ========= UI methods ============ //

          acceptAll: function(){
            this.quotes.forEach( function(quote){
              quote.selected = true;
            });
          },

          declineAll: function(){
            this.quotes.forEach( function(quote){
              quote.selected = false;
            });
          },

          resetAll: function(){
            this.quotes.forEach( function(quote){
              quote.selected = null;
            });
          },

          // initialize the data table with the ugliest function ever.
          initializeDataTable: function(){
            $('#requirement_table').DataTable( {
              initComplete: function () {
                
                  this.api().columns().every( function () {
                    var footer = this.footer();
                    if(! this.footer().includes('<div')) {
                      var column = this;
                      var select = $('<select><option value=""></option></select>')
                          .appendTo( $(column.footer()).empty() )
                          .on( 'change', function () {
                              var val = $.fn.dataTable.util.escapeRegex(
                                  $(this).val()
                              );
       
                              column
                                  .search( val ? '^'+val+'$' : '', true, false )
                                  .draw();
                          } );
       
                      column.data().unique().sort().each( function ( d, j ) {
                          select.append( '<option value="'+d+'">'+d+'</option>' )
                      } );

                    }
                  } );

                }
              });
            },

          setStagedQuotes: function() {
            var vm = this;

            this.quotes.forEach( function( quote ){
             
                vm.stagedQuotes.forEach( function(stagedQuote){
                  if( stagedQuote.id == quote.id ){
                    
                    if( quote.status == 'accepted'){
                      quote.selected = true;
                    }
                  
                    if( quote.status == 'declined'){
                      quote.selected = false;
                    }
                  }
                });
            });
          },


    // ========= API Calls ============ //

          getStagedQuotes: function(){
            var vm = this;
            axios.get('{!! route("quotes.staged") !!}', { headers:  { 'X-CSRF-TOKEN': '{!! csrf_token() !!}', }})
            .then( function(response){
              console.log(response.data);
              vm.stagedQuotes = response.data;
              vm.setStagedQuotes();
              vm.quotes.forEach( function( quote ){
                vm.stagedQuotes.forEach(function(stagedQuote)
                {
                  if (quote.id == stagedQuote.id)
                  {
                    Vue.set(quote, 'selected', true);
                    
                  }
                });
              });
              vm.quoteSync = false;
          }).catch(error => {
              if (error ) {
                  console.log(error);
              } 
            });
          },

          getQuote: function(quote){
              axios.get('{!! url("/") !!}/quote/sync', quote, 
              { headers: 
                { 'X-CSRF-TOKEN': '{!! csrf_token() !!}', }
            }).then( function(response){
                quote = response;
        }).catch(error => {
            if (error ) {
                console.log(error);
            } 
          });
          },

          // iterate over quotes and set their status before sync
          setQuoteStatuses: function(quotes) {
            quotes.forEach(function(quote){
        
              if(quote.selected == true ){
                quote.status = 'accepted';
              }

              if (quote.selected === false){
                quote.status = 'declined';
              }

              if (quote.selected === null ){
                quote.status = 'open';
              }
            });
          },

          getAllApprovedQuotes: function(){
            var approved = [];
            this.quotes.forEach(function( quote ){
              if( quote.selected == true){
                approved.push(quote);
              }
            });
            return approved;
          },

          inArray: function(needle, haystack) {
            var length = haystack.length;
            for(var i = 0; i < length; i++) {
                if(haystack[i] == needle) return true;
            }
            return false;
        },

          // sync the quotes.
          syncQuotes: function(){
            var vm = this;
            this.setQuoteStatuses(this.quotes);
            var quotes = this.getAllApprovedQuotes();
            console.log(quotes);
            vm.quoteSync = true;
            axios.post('{!! route("quote.sync") !!}', quotes, 
              { headers: 
                { 'X-CSRF-TOKEN': '{!! csrf_token() !!}', }
            }).then( function( response ){
              
              vm.getStagedQuotes();
              vm.setStagedQuotes();
        }).catch(error => {
            if ( error ) {
                vm.quoteSync = false;
                console.log(error);
            } 
          });

        },
        }
      })
    </script>
    <style>
      .bold-selected-quote {
        font-weight: 700;
      }
    </style>
    <style>
        .bg-custom {
            background: #8360c3;  /* fallback for old browsers */
    background: -webkit-linear-gradient(to left, #2ebf91, #8360c3);  /* Chrome 10-25, Safari 5.1-6 */
    background: linear-gradient(to left, #2ebf91, #8360c3); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
    color: #fff !important;
    
        }

        /* Absolute Center Spinner */
.loading {
  position: fixed;
  z-index: 999;
  height: 2em;
  width: 2em;
  overflow: show;
  margin: auto;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
}

/* Transparent Overlay */
.loading:before {
  content: '';
  display: block;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0,0,0,0.3);
}

/* :not(:required) hides these rules from IE9 and below */
.loading:not(:required) {
  /* hide "loading..." text */
  font: 0/0 a;
  color: transparent;
  text-shadow: none;
  background-color: transparent;
  border: 0;
}

.loading:not(:required):after {
  content: '';
  display: block;
  font-size: 10px;
  width: 1em;
  height: 1em;
  margin-top: -0.5em;
  border-radius: 0.5em;
  -webkit-box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.5) -1.5em 0 0 0, rgba(0, 0, 0, 0.5) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
  box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) -1.5em 0 0 0, rgba(0, 0, 0, 0.75) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
}
    </style>
@endsection
