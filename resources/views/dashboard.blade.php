@extends('layouts.app')

@section('content')

@include('partials._sidenav')

         <!-- Page content-->
         <main id="jetbid_dashboard" class="main" style="padding-top:20px;">
         <div class="container-fluid" style="min-height:800px;">
            <div class="row">
                  
               <!-- Blog Content-->
               <div class="col-md-9">
                  @yield('dashboard-content')
               </div>
               <!-- Blog Sidebar-->
               <div class="col-md-3">
                  @yield('dashboard-right-sidebar')
               </div>
            </div>
         </div>
        </main>

@endsection