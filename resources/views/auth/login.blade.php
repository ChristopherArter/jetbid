@extends('layouts.app')

@section('content')





<div class="wrapper">
    <div class="block-center mt-4 wd-xl">
       <!-- START card-->
       <div class="card card-flat">
          <div class="card-header text-center">
             
                    <img src="/images/jetbid_logo.svg" alt="jetbid logo" width="180">

          </div>
          <div class="card-body">
             <p class="text-center py-2">Please sign-in to continue.</p>
             <form class="mb-3" id="loginForm" novalidate method="POST" action="{{ route('login') }}">
                    
                            @csrf
                <div class="form-group">


                   <div class="input-group with-focus">
                      <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} border-right-0" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
                      <div class="input-group-append">
                         <span class="input-group-text fa fa-envelope text-muted bg-transparent border-left-0"></span>
                      </div>
                      @if ($errors->has('email'))
                      <span class="invalid-feedback">
                          <strong>{{ $errors->first('email') }}</strong>
                      </span>
                  @endif
                   </div>
                </div>
                <div class="form-group">
                   <div class="input-group with-focus">

                      <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} border-right-0" name="password" required id="password" type="password" placeholder="Password">
                      <div class="input-group-append">
                         <span class="input-group-text fa fa-lock text-muted bg-transparent border-left-0"></span>
                      </div>

                      @if ($errors->has('password'))
                      <span class="invalid-feedback">
                          <strong>{{ $errors->first('password') }}</strong>
                      </span>
                  @endif
                   </div>
                </div>
                <div class="clearfix">
                   <div class="checkbox c-checkbox float-left mt-0">
                      <label>
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> <span class="fa fa-check"></span>Remember Me</label>
                         
                   </div>
                   <div class="float-right"><a class="text-muted" href="{{ route('password.request') }}">Forgot your password?</a>
                   </div>
                </div>
                <button class="btn btn-block btn-primary mt-3" type="submit">Login</button>
             </form>
             <p class="pt-3 text-center">Need to Signup?</p><a class="btn btn-block btn-secondary" href="{{ route('register') }}">Register Now</a>
          </div>
       </div>
       <!-- END card-->
       <div class="p-3 text-center">
          <span class="mr-2">&copy;</span>
          <span>2018</span>
          <span class="mr-2">-</span>
          <span>JetBid LLC</span>
         
       </div>
    </div>
 </div>
@endsection
