<li class=" ">
    <a href="#multilevel" title="Multilevel" data-toggle="collapse">
       <em class="fa fa-folder-open-o"></em>
       <span>Multilevel</span>
    </a>
    <ul class="sidebar-nav sidebar-subnav collapse" id="multilevel">
       <li class="sidebar-subnav-header">Multilevel</li>
       <li class=" ">
          <a href="#level1" title="Level 1" data-toggle="collapse">
             <span>Level 1</span>
          </a>
          <ul class="sidebar-nav sidebar-subnav collapse" id="level1">
             <li class="sidebar-subnav-header">Level 1</li>
             <li class=" ">
                <a href="multilevel-1.html" title="Level1 Item">
                   <span>Level1 Item</span>
                </a>
             </li>
             <li class=" ">
                <a href="#level2" title="Level 2" data-toggle="collapse">
                   <span>Level 2</span>
                </a>
                <ul class="sidebar-nav sidebar-subnav collapse" id="level2">
                   <li class="sidebar-subnav-header">Level 2</li>
                   <li class=" ">
                      <a href="#level3" title="Level 3" data-toggle="collapse">
                         <span>Level 3</span>
                      </a>
                      <ul class="sidebar-nav sidebar-subnav collapse" id="level3">
                         <li class="sidebar-subnav-header">Level 3</li>
                         <li class=" ">
                            <a href="multilevel-3.html" title="Level3 Item">
                               <span>Level3 Item</span>
                            </a>
                         </li>
                      </ul>
                   </li>
                </ul>
             </li>
          </ul>
       </li>
    </ul>
 </li>