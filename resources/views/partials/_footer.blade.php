
<!-- Scripts -->

{{--  CDNs  --}}
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

  <script src="{{ asset('js/app.js') }}"></script>
  @yield('footer-scripts')

<footer class="app-footer" style="margin-top:30px;">
  <div>
    <span>&copy; <?php echo date("Y");?> JetBid LLC</span>
  </div>
  <div class="ml-auto">
    <span>JetBid is a </span>
    <a href="https://flightmotionav.com">Flight Motion</a> company
  </div>
</footer>
