<div class="sidebar">
        <nav class="sidebar-nav">
          <ul class="nav">

    {{--  REQUIREMENTS  --}}

            <li class="nav-title">Requirements</li>
            <li class="nav-item">
                    <a class="nav-link" href="#">
                            <i class="nav-icon fas fa-clock"></i>
                        Recent
                        <span class="badge badge-primary">4</span>
                    </a>
            </li>

            <li class="nav-item">
                <a class="nav-link
                <?php if( url()->current() == route('requirements.open') ){
                        echo ' nav-link-primary';
                    }?>
                " href="{{ route('requirements.open') }}">
                        <i class="nav-icon fas fa-door-open"></i>
                    Open
                    
                </a>
            </li>
            
            {{--  IF AIRCRAFT APPROVED  --}}

            @if(Auth::user()->isAircraftApproved() )
            <li class="nav-item">
                    <a class="nav-link 
                    <?php

                    if( url()->current() == route('requirements.quotes') ){
                        echo ' nav-link-primary';
                    }?>" href="{!! route('requirements.quotes') !!}">
                            <i class="nav-icon fas fa-inbox"></i>
                        Inbox
                    </a>
                </li>
            <li class="nav-item">
                    <a class="nav-link" href="#">
                            <i class="nav-icon fas fa-plus-square"></i>
                        New Requirement
                    </a>
                </li>
                @endif

            <li class="nav-item">
                    <a class="nav-link" href="#">
                            <i class="nav-icon fas  fa-search"></i>
                        Advanced Search
                     
                        
                    </a>
                </li>
                
        {{--  YOUR QUOTES  --}}

        @if( Auth::user()->membershipIsActive() )
                <li class="nav-title ">Quotes</li>
                <li class="nav-item">
                        <a class="nav-link" href="#">
                                <i class="nav-icon fas fa-trophy"></i>
                            Approved
                            <span class="badge badge-success">3</span>
                        </a>
                    </li>

                    <li class="nav-item">
                            <a class="nav-link" href="#">
                                    <i class="nav-icon fas fa-share-square"></i>
                                Submitted
                                <span class="badge badge-default">16</span>
                            </a>
                    </li>

                    <li class="nav-item">
                            <a class="nav-link" href="#">
                                    <i class="nav-icon fas fa-door-closed"></i>
                                Closed
                                
                            </a>
                    </li>


                    <li class="nav-item">
                            <a class="nav-link" href="#">
                                    <i class="nav-icon fas fa-cloud-download-alt"></i>
                                Export
                               
                            </a>
                    </li>
        @endif


        {{--  YOUR AIRCAFT  --}}

                @if( Auth::user()->membershipIsActive() )
                <li class="nav-title ">Fleet</li>
                <li class="nav-item">
                        <a class="nav-link" href="#">
                                <i class="nav-icon fas fa-plane"></i>
                            Your Aircraft
                            <span class="badge badge-success">3</span>
                        </a>
                    </li>

                    <li class="nav-item">
                            <a class="nav-link" href="#">
                                    <i class="nav-icon fas fa-plus-square"></i>
                                Add Aircraft
                            </a>
                        </li>
        @endif
          </ul>
        </nav>
        <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>