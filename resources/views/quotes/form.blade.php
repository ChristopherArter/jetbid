
<h3>Requirement</h3>

<div class="input-group mb-3">
        <div class="input-group-prepend">
           <span class="input-group-text">$</span>
        </div>
        <input class="form-control" type="text" aria-label="Quoted Price" value="{{ $quote->price or '' }}" >
        <div class="input-group-append">
           <span class="input-group-text">USD</span>
        </div>
</div>

<div class="col-md-10">
        <input class="form-control" type="text" value="{{ $quote->lead_time or '' }}" placeholder="Lead time">
        <span class="form-text">(Optional)</span>
</div>

<div class="col-md-10">
                <textarea class="form-control" aria-label="With textarea" value="{{ $quote->description or '' }}"></textarea>
                <span class="form-text">(Optional)</span>
</div>
