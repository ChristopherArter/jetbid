@extends('dashboard')
@section('dashboard-content')
<requirement-details-card :requirement="requirement"></requirement-details-card>
<hr>
<?php $parentQuote = $requirement->getQuoteByUser(Auth::user()); ?>


<div class="row" style="margin-bottom:20px;">

    <div class="col-sm-9">
        <h4>Quote this Requirement</h4>
    </div>
    <div class="col-sm-3">
        <button v-if="quotes.length > 0" @click="saveQuoteOptions()" class="btn btn-primary float-right">
            <span v-if="!sync"><i class="fas fa-save"></i> Save All</span>
            <i v-if="sync" class="fas fa-lg fa-spinner fa-spin"></i>
        </button>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
            <div v-for="quote in quotes" class="card">
                <quote-form
                ref="quoteOptions"
                v-if="quote" 
                :quote="quote" 
                :requirement="requirement" 
                v-on:quote-option-removed="removeQuote($event)"
                v-on:quote-updated="quote = $event"></quote-form>
            </div>

    </div>
</div>
<div class="row">
        <div class="col-sm-12 text-center">
                <button @click="addQuote" class="btn btn-primary"><i class="fas fa-plus"></i> Add Quote Option</button>
        </div>
    </div>

@endsection
@section('dashboard-right-sidebar')
<div v-if="!submitted && quotes.length > 0" class="card">
    <div class="card-body">
        @{{ quote.id }}
        <button @click="finalPromptQuotes()" class="btn btn-primary btn-lg">
            
            Submit
        </button>
    </div>
</div>

    <requirement-quote-status-box
    v-if="submitted && quotes.length > 0"
    :quote="requirement.quotes[0]"></requirement-quote-status-box>

@endsection
@section('footer-scripts')
<script>

    var singleReq = new Vue({
        el: '#jetbid_dashboard',
        data: {
            requirement: {!! $requirement !!},
            quotes: [],
            baseurl: '{!! url('/') !!}',
            sync: false,
            changed: false,
            showSavedAlert: false,
            quote: [],
            
        },
        mounted()
        {

            this.getQuote();
        },

        computed:{
            submitted: function(){
                vm = this;
                if(vm.requirement.quotes[0] !== undefined)
                {
                    if(vm.requirement.quotes[0].status == 'submitted')
                    {
                        return true;
                    }
                }
            },

            quote: function(){
                if(quotes.length < 1 ){
                    this.quote = null;
                } else {
                    quote: this.quote;
                }
            }
        },
        methods: {
            addQuote: function()
            {
                vm = this;
                
                this.quotes.push({part_number: vm.requirement.part_number});
                
            },

            removeQuote: function(quote){

                var vm = this;
                // remove quote from array
                var index = vm.quotes.indexOf(quote);
                if (index > -1) {
                vm.quotes.splice(index, 1);
                }
            },

            saveQuoteOptions: function()
            {
                toastr.info('Saving...');
                var vm = this;
                vm.$refs.quoteOptions.forEach(function( quote ){
                    quote.saveSingleQuote(false);
                });
                
            },

            submitQuotes: function(){
                var vm = this;
                vm.sync = true;
                var submitToastr = toastr.info('Saving');
                
                axios.post('/quote/' + '{!! $requirement->id !!}' + '/store', vm.quotes, 
                  { headers: 
                    { 'X-CSRF-TOKEN': '{!! csrf_token() !!}', }
                }).then( function( response ){
                vm.quotes = response.data.quote_options;
                vm.sync = false;
                vm.showSavedAlert = true;
                toastr.clear();
                toastr.success('Saved')
            }).catch(error => {
                if ( error ) {
                    console.log(error);
                } 
              });
            },

            getQuote: function()
            {
                var vm = this;
                vm.sync = true;
                axios.get('{!! url("/") !!}' + '/json/quote/' + '{!! $requirement->id !!}',
                { headers: 
                  { 'X-CSRF-TOKEN': token, }
              }).then( function( response ){
              vm.quote = response.data;
              if(vm.quote)
              {
                vm.quotes = vm.quote.quote_options;
              }
              

              vm.sync = false;
              vm.changed = false;
          }).catch(error => {
              if ( error ) {
                  console.log(error);
              } 
            });
            },



            getRequirement: function()
            {
                vm = this;
                axios.get( vm.baseurl + '/requirements/' + '{!! $requirement->id !!}' + '/show/json',
                { headers: 
                  { 'X-CSRF-TOKEN': '{!! csrf_token() !!}', }
              }).then( function( response ){
              vm.requirement = response.data;
          }).catch(error => {
              if ( error ) {
                  console.log(error);
              } 
            });
            },

            finalSendQuotes: function(){
                var vm = this;
                var quote = quote;
                axios.post( vm.baseurl + '/quote/submit/' + vm.requirement.id, 
                  { headers: 
                    { 'X-CSRF-TOKEN': '{!! csrf_token() !!}', }
                }).then( function( response ){
                    vm.getRequirement();
                    return swal("Quotes Sent!");
                
            }).catch(error => {
                if ( error ) {
                    console.log(error);
                } 
              });

            },

            finalPromptQuotes: function()
            {
                vm = this;
               
                swal({
                    text: 'Ready to submit?',
                    button: {
                      text: "Yes!",
                      closeModal: false,
                    },
                  })
                  .then( function(){
                    vm.finalSendQuotes();
                  })
                  .then(function(){
                
                  }).catch(err => {
                    if (err) {
                        console.log(err);
                      swal("Error!", "error");
                    } else {
                      swal.stopLoading();
                      swal.close();
                    }
                  });
            }
        }
    });
</script>

@endsection