<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuoteOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quote_options', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('quote_id');
            $table->string('description')->nullable();
            $table->string('lead_time')->nullable();
            $table->string('status')->default('open');
            $table->float('price');
            $table->string('condition')->nullable();
            $table->boolean('closed')->default(false);
            $table->string('quote_option_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quote_options');
    }
}
