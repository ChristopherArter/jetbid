<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequirementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requirements', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->dateTime('end');
            $table->longText('description');
            $table->string('part_number');
           //$table->json('condition')->nullable();
            //$table->json('type')->nullable();
            $table->boolean('aog')->default(0);
            $table->string('status')->nullable();
            $table->string('location');
            $table->integer('aircraft_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requirements');
    }
}
