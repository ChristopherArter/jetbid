<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Users
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'defaultOptions'    =>  [

        // how often they get emails.
        'updates'   =>  ['weekly'],

        // shipping info.
        'shipping'  =>  [

            'carriers'  =>  [
                [
                    'label' =>  'UPS',
                    'account_number'    =>  '1234567',
                    'default' =>  false,
                    'rate'  =>  'ground'
                ],
                [
                    'label' =>  'FedEx',
                    'account_number'    =>  '1234567',
                    'default' =>  true,
                    'rate'  =>  'ground'
                ]
            ],

            'addresses' =>  [

                [ 
                    'label'         =>  'Home',
                    'default'           => false,
                    'street_address'    =>  '846 Jaybee Ave',
                    'street_address_2'  =>  '',
                    'state' =>  'Florida',
                    'zip'   =>  '33897',
                    'Country'   =>  'US' ],

                    [ 
                    'label'         =>  'Head Quarters',
                    'default'           => true,
                    'street_address'    =>  '123 Main St.',
                    'street_address_2'  =>  'Unit 456',
                    'state' =>  'Florida',
                    'zip'   =>  '33897',
                    'Country'   =>  'US' ]

            ]

        ],

        'terms' =>  [
            [
                'label' =>  'Purchase Terms',
                'content'   =>  'This is the purchase order Terms and Conditions',
                'type'  =>  'purchase'
            ],

            [
                'label' =>  'Quote Terms',
                'content'   =>  'these are the quote terms',
                'type'  =>  'Quote'
            ]
        ],
        'contacts'  =>  [
            
            [
                'label' =>  'Shipping',
                'name'  =>  'Mr Anderson',
                'email' =>  'anderson@neosucks.com',
                'position'  =>  'shipping'
            ],

             [
                'label'    =>  'Sales',
                'name'  =>  'Sales Guy 1',
                'email' =>  'salesguy1@salesguy.com',
                'position'  =>  'sales dude'
            ]
        ],
    ]

];
