<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Quote standard config values.
    |--------------------------------------------------------------------------
    |
    */

    'cache' => [
        'keys' => [
            'forReview' => 'quotesReadyForReview'
        ],
    ],
    'quote_prefix'  =>  'QUOTE-',
    'quote_option_prefix'   =>  'OPTION-',
];
