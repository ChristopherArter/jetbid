<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Auth::routes();
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::resource('requirement', 'RequirementController')->middleware('auth');

Route::get('/requirement/{id}', 'RequirementController@show')->middleware('auth');
Route::resource('requirementtype','RequirementTypeController')->middleware('auth');
Route::get('/requirements/json', 'RequirementController@json')->middleware('auth');

/** 
 *   AUTH PROTECTED ROUTES
 */
Route::group(['middleware' => ['auth']], function()
{
    Route::get('/', function () {
        return view('dashboard');
    });
    Route::get('/requirements/search', 'RequirementController@search')->name('requirement.search');    
});

/**
 *  RESOURCES
 */
Route::resource('requirement', 'RequirementController',
                ['only' => ['index', 'show']])->middleware('auth');

Route::resource('requirement', 'RequirementController',
    ['except' => ['create', 'store', 'update', 'destroy']])->middleware(['auth']);

    Route::get('requirement/types', 'RequirementTypeController@index')->name('types.all');

    Route::prefix('requirement')->name('requirement.')->group(function () {
        Route::get('{id}/types', 'RequirementController@types')->name('types');
        Route::get('{id}/quotes', 'RequirementController@quotes')->name('quotes');
       
    });

    Route::prefix('requirements')->name('requirements.')->group(function () {

        Route::get('open', 'RequirementController@index')->name('open')->middleware(['auth']);
        Route::get('recent', 'RequirementController@getRecent')->name('recent')->middleware(['auth']);
        Route::get('quotes', 'RequirementController@getRecievedQuotes')->name('quotes')->middleware(['auth']);
        Route::get('{requirement}/show/json', 'RequirementController@getJson')->name('get.json')->middleware(['auth']);
       
    });

    /**
     *  QUOTE ROUTES
     */

    Route::prefix('quote')->name('quote.')->group(function () {

        Route::post('delete/{id}', 'QuoteController@delete')->name('delete')->middleware(['auth']);
        Route::get('/{id}', 'QuoteController@quotes')->name('show')->middleware(['auth']);
        Route::post('/sync','QuoteOptionController@sync')->name('sync')->middleware(['auth']);
        Route::get('/{requirementId}/new', 'QuoteController@create')->name('create')->middleware(['auth']);
        Route::post('/{requirementId}/store', 'QuoteController@store')->name('store')->middleware(['auth']);
        Route::post('submit/{requirementId}', 'QuoteController@submit')->name('submit')->middleware(['auth']);
        

        /**
         *  QUOTE OPTIONS
         */
        Route::get('/{requirementId}/options', 'QuoteOptionController@listByRequirement')->name('options')->middleware(['auth']);
        Route::post('/{requirementId}/options/{optionId}/delete', 'QuoteOptionController@destroy')->name('option.delete')->middleware(['auth']);
        Route::post('options/store', 'QuoteOptionController@store')->name('store')->middleware(['auth']);
    });

        
    /**
     *  JSON ROUTE PREFIX
     */
    
        Route::prefix('json/')->name('requirement.')->group(function () {

            /**
             *  Quote options
             */
            Route::post('quoteoption/{requirementId}/store', 'QuoteOptionController@store')->middleware(['auth']);
            Route::delete('quoteoption/{quoteoption}', 'QuoteOptionController@destroy')->middleware(['auth']);
            Route::post('quoteoption/{quoteOption}/update', 'QuoteOptionController@update')->middleware(['auth']);
           // Route::post('quoteoption/update/{quoteoption}', 'QuoteOptionController@update')->name('quoteoption.update')->middleware(['auth']);
        
            /**
             * 
             *  Quotes
             */
            Route::get('/quote/{requirement}', 'QuoteController@getQuoteByRequirement')->middleware(['auth']);
        });
    
    Route::prefix('quotes')->name('quotes.')->group(function () {

        Route::get('review', 'QuoteController@review')->name('review')->middleware(['auth']);
        Route::get('staged', 'QuoteController@getStaged')->name('staged')->middleware(['auth']);
        Route::get('review/confirmation', 'QuoteController@getConfirmation')->name('confirmation')->middleware(['auth']);
        
    
    });
    

    /**
     *  USER ROUTES
     */

    Route::prefix('user')->name('user.')->group(function () {

        Route::get('quotes', 'UserController@getQuotes')->name('quotes')->middleware(['auth']);
        Route::get('aircraft', 'UserController@getAircraft')->name('aircraft')->middleware(['auth']);
        Route::get('subscriptions','UserController@getSubscriptions')->name('subscriptions')->middleware(['auth']);
        Route::get('profile','UserController@getProfile')->name('profile')->middleware(['auth']);
        Route::get('dashboard', 'UserController@dashboard')->name('dashboard')->middleware(['auth']);
    });