<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequirementStatus extends Model
{

    public function requirements(){
        return $this->belongsToMany('App\Requirement');
    }
}
