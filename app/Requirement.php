<?php

namespace App;
use App\Aircraft;
use App\RequirementType;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Requirement extends Model
{

    protected $table = 'requirements';
    
   public function status(){
       return $this->belongsToMany('App\RequirementStatus');
   }

   public function types(){
    return $this->belongsToMany('App\RequirementType','requirement_requirementtypes', 'requirement_id', 'requirementtype_id');
   }

   public function aircraft(){
       return $this->belongsTo('App\Aircraft');
   }

   public function quotes(){
       return $this->hasMany('App\Quote');
   }

   public function getQuoteByUser($user)
   {
        foreach($this->quotes as $quote)
        {
            if ($quote->user_id == $user->id)
            {
                return $quote;
            }
        }
   }

}
