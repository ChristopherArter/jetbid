<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aircraft extends Model
{
    protected $table = 'aircrafts';

    public function requirements(){
        return $this->hasMany('App\Requirement');
    }
    
}
