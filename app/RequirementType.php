<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequirementType extends Model
{
    protected $table = 'requirementtypes';
    public function requirements(){
        return $this->belongsToMany('App\Requirement', 'requirement_requirementtypes', 'requirement_id', 'requirementtype_id');
    }
}
