<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use App\Requirement;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];
    protected $casts = [
        'capabilities' => 'array',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    // protected $casts = [
    //     'options' => 'array',
    // ];
    public function setUserOptions(array $array ){
        $this->options = json_encode($array, true);
        $this->save();
    }
    public function quotes(){
        return $this->hasMany('App\Quote');
    }

    public function requirements(){
        return $this->hasManyThrough('App\Requirement','App\Aircraft');
    }

    public function aircrafts(){
        return $this->hasMany('App\Aircraft');
    }

    public function membershipIsActive(){
        if( $this->membership_status != 'canceled'  ){
            return true;
        }
    }

    public function isAircraftApproved(){
        
        if($this->aircraft_approved == true){
            return true;
        }
    }

    public function hasQuoted( Requirement $requirement ){
       foreach( $requirement->quotes as $quote ){
           if($quote->user_id == $this->id) {
               return true;
           }
       }
       return false;
    }

    public function getOptions(){
        return $this->options;
    }

    /**
     * Check if the user has capabilitles for this part. 
     *
     * @param Requirement $requirement
     * @return boolean
     */
    public function hasCapabilities(string $partNumber){
        if( in_array( $partNumber, $this->capabilities ) )
        {
            return true;
        }
    }

}
