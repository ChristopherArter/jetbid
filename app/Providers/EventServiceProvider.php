<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Event' => [
            'App\Listeners\EventListener',
        ],

        /**
         *  QUOTE APPROVED
         */

        'App\Events\QuoteConfirmed' => [   
            'App\Listeners\QuoteConfirmedListener'
        ],

        /**
         *  REQUIREMENT POSTED
         */
        'App\Events\NewRequirement' => [   
            'App\Listeners\NewRequirementNotificationListener',
            'App\Listeners\NewRequirementUserCapabilitiesCheck'
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
