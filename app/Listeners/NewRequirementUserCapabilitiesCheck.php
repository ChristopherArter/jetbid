<?php

namespace App\Listeners;

use App\Events\NewRequirement;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Auth;
use App\User;

class NewRequirementUserCapabilitiesCheck
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewRequirement  $event
     * @return void
     */
    public function handle(NewRequirement $event)
    {
        $users = User::all();

        foreach( $users as $user )
        {
            if( $user->hasCapabilities( $event->part_number ) ){

                $data = [
                    'user'          =>  $user,
                    'requirement'   =>  $event
                ];
                SendRequirementNotificationEmail::dispatch($data);
    
            }
        }
    }
}
