<?php

namespace App\Listeners;

use App\Events\NewRequirement;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewRequirementNotificationListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewRequirement  $event
     * @return void
     */
    public function handle(NewRequirement $event)
    {
        //
    }
}
