<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Auth;
use App\Requirement;
use Carbon\Carbon;
use App\Events\QuoteConfirmed;
use App\QuoteOption;


class Quote extends Model
{
   protected $table = 'quotes';

   /**
    * Link User to Quote model.
    *
    * @return void
    */
   public function user(){
       return $this->belongsTo('App\User');
   }

   /**
    * Get the quote requirement
    *
    * @return void
    */
   public function requirement(){
       return $this->belongsTo('App\Requirement');
   }
   
   public function quoteOptions(){
       return $this->hasMany('App\QuoteOption');
   }

   /**
    * Return aircraft this quote is ultimately for.
    *
    * @return void
    */
   public function aircraft(){
       $requirement = $this->requirement;
       return $requirement->aircraft;
   }

   /**
    * Get the type of quote model.
    *
    * @return void
    */
   public function type(){
       return $this->belongsTo('App\RequirementType');
   }

   /**
    * Test whether this quote is open or not
    *
    * @return boolean
    */
   public function isOpen(){
       if ($this->status == 'open'){
           return true;
       }
       return false;
   }

   /**
    * Test whether this quote is accepted or not.
    *
    * @return boolean
    */
   public function isAccepted(){
       if( $this->status == 'accepted' ){
           return true;
       }
   }

    /**
    * Test whether this quote is declined or not.
    *
    * @return boolean
    */
    public function isDeclined(){
        if( $this->status == 'declined' ){
            return true;
        }
    }

    /**
     * Set the status of a quote. accepted, declined or open.
     *
     * @param string $status
     * @return void
     */
    protected function setStatus( string $status, $save = true ){
        $this->status = $status;
        if( $save ){
            $this->save();
        }
    }

    /**
     * Approve the quote.
     *
     * @param boolean $save
     * @return void
     */
    public function approve( $save = true ){
        $this->setStatus('accepted', $save);
    }

    /**
     * Decline the quote
     *
     * @param boolean $save
     * @return void
     */
    public function decline( $save = true ){
        $this->setStatus('declined', $save);
    }

    /**
     * Set status to open
     *
     * @param boolean $save
     * @return void
     */
    public function open( $save = true ){
        $this->setStatus('open', $save);
    }

   /**
    * Takes array and maps it to an existing model.
    *
    * @param array $quote
    * @return void
    */
   public function syncModel(  $quote ){

    $user = Auth::user();

    if( isset( $quote['created_at' ] ) ) {

        // TO DO: Make this a middleware
       if( $this->user_id == $user->id ) {

        $this->updated_at = Carbon::now()->toDateTimeString();
        $this->description = $quote['description'];
        $this->lead_time = $quote['lead_time'];
        $this->user_id = Auth::user()->id;
        $this->user = Auth::user();
        $this->status = $quote['status'];
        $this->price = (int) $quote['price'];
        $this->requirement_type_id = $quote['requirement_type_id'];
        $this->condition_id = (int) $quote['condition_id'];

        return $this;
       }
    }
   }

   /**
    * Confirm the quote's status.
    *
    * @return void
    */
   public function confirm( $confirm = true ){
    if($confirm){
        $this->confirmed = true;
        $this->save();
        event(new QuoteConfirmed($this));
    } else {
        $this->confirmed = false;
    }
   }

   public function deleteAllOptions()
   {
       foreach($this->quoteOptions as $option)
       {
           $option->delete();
       }
   }
}
