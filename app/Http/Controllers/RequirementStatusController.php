<?php

namespace App\Http\Controllers;

use App\RequirementStatus;
use Illuminate\Http\Request;

class RequirementStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RequirementStatus  $requirementStatus
     * @return \Illuminate\Http\Response
     */
    public function show(RequirementStatus $requirementStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RequirementStatus  $requirementStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(RequirementStatus $requirementStatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RequirementStatus  $requirementStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RequirementStatus $requirementStatus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RequirementStatus  $requirementStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(RequirementStatus $requirementStatus)
    {
        //
    }
}
