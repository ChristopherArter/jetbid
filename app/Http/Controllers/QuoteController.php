<?php

namespace App\Http\Controllers;

use App\Quote;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use App\Requirement;
use App\QuoteOption;

class QuoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($requirementId)
    {
       $requirement = Requirement::find($requirementId);
       $quoteOptions = collect();
       if($quote = $requirement->getQuoteByUser(Auth::user()))
       {

         foreach($quote->quoteOptions as $quoteOption)
         {
             $quoteOptions->push($quoteOption);
         }

       }

       return view('quotes.create-edit-quotes')->with('requirement', $requirement)->with('quotes', $quoteOptions);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( $requirementId, Request $request)
    {
        $requirement = Requirement::find($requirementId);
        if($requirement)
        {
            // If we already have an existing quote, let's use that.
            if( $quote = $requirement->getQuoteByUser(Auth::user()))
            {
                $quote;
            
            // if no quote, let's create a new one.
            } else {
                $quote = new Quote();
                $quote->confirmed = false;
                $quote->status = 'open';
                $quote->user_id = Auth::user()->id;
                $quote->requirement_id = $requirement->id;
                $quote->save();
                $requirement->quotes()->save($quote);
            }

           foreach($request->all() as $quoteOption)
           {
               if( isset($quoteOption['id']) && $newQuoteOption = QuoteOption::find((int) $quoteOption['id'] ) ){
                    $newQuoteOption;
               } else 
               {
                    $newQuoteOption = new QuoteOption();
               }
                $newQuoteOption->price =  (float) $quoteOption['price'];
                $newQuoteOption->description = $quoteOption['description'];
                $newQuoteOption->part_number = $quoteOption['part_number'];
                $newQuoteOption->condition = $quoteOption['condition'];
                $newQuoteOption->lead_time  = $quoteOption['lead_time'];
                $newQuoteOption->closed = false;
                $newQuoteOption->setQuoteOptionId();
                $quote->quoteOptions()->save($newQuoteOption);

           }

           return $quote->load('quoteOptions');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Quote  $quote
     * @return \Illuminate\Http\Response
     */
    public function show(Quote $quote)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Quote  $quote
     * @return \Illuminate\Http\Response
     */
    public function edit(Quote $quote)
    {
        //
    }

    /**
     * 
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Quote  $quote
     * @return \Illuminate\Http\Response
     */
    public function sync( Request $request )
    {
        $sessionKey = 'quotesStagedForResponse';
        $returnQuotes = collect();
        foreach( $request->all() as $inboundQuote ){
            $quoteOption = QuoteOption::find((int) $inboundQuote['id'])->load('quote');
            $requirement = $quoteOption->quote->requirement;
            $aircraft = $requirement->aircraft;
            $quoteOption->syncModel($inboundQuote);
            $quoteOption->requirement = $requirement;
            $quoteOption->requirement->aircraft = $aircraft;

            $returnQuotes->push($quoteOption);
        }
        $request->session()->put( $sessionKey, $returnQuotes);
        return $returnQuotes;
    }
    
    /**
     * get the session for staged quotes.
     *
     * @return void
     */
    public function getStaged( Request $request ){
        $quotes = session('quotesStagedForResponse');
        
        return $quotes;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Quote  $quote
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        dump($id);
        $quote::findOrFail($id);
        $quote->delete();
        return true;
    }

    /**
     * Start the final quote approval wizard
     *
     * @return void
     */
    public function review(){

        $quotes = session('quotesStagedForResponse');

        if( $quotes ) {

            return view('requirements.quote-approval-wizard')
            ->with('quotes', $quotes->where('status', '==', 'accepted' ))
            ->with('user', Auth::user());

        } else {
            return redirect()->back();
        }
    }

    public function getConfirmation(Request $request){
        $quotes = session('quotesStagedForResponse');
        $savedQuotes = collect();
        if($quotes){
            
            foreach($quotes as $cachedQuote){
                $quote = QuoteOption::findOrFail($cachedQuote->id);
                //$quote->confirm();
                $savedQuotes->push($quote);
                
            }
        }
        $request->session()->forget('quotesStagedForResponse');
        $request->session()->flash('confirmedQuotes', $savedQuotes);
        return view('requirements.quote-approval-confirmation')->with('quotes', session('confirmedQuotes')->toJson());
    }


    public function submit($requirementId, Request $request)
    {
        $requirement = Requirement::findOrFail($requirementId);
        $quote = $requirement->getQuoteByUser(Auth::user());
        if($quote)
        {
            $quote->status = 'submitted';
            $quote->submitted_at = Carbon::now();
            $quote->save();
            // fire event here
            return $quote;
        }
    }

    /**
     * Get quote by requirement model
     *
     * @param Requirement $requirement
     * @return Quote
     */
    public function getQuoteByRequirement( Requirement $requirement){
        $quote = $requirement->getQuoteByUser(Auth::user());
        if($quote)
        {
            $quote->load('quoteOptions');
            return $quote;
        }
    }
}
