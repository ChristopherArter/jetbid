<?php

namespace App\Http\Controllers;

use App\RequirementType;
use Illuminate\Http\Request;

class RequirementTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = RequirementType::all();
        return $types;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RequirementType  $requirementType
     * @return \Illuminate\Http\Response
     */
    public function show(RequirementType $requirementType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RequirementType  $requirementType
     * @return \Illuminate\Http\Response
     */
    public function edit(RequirementType $requirementType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RequirementType  $requirementType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RequirementType $requirementType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RequirementType  $requirementType
     * @return \Illuminate\Http\Response
     */
    public function destroy(RequirementType $requirementType)
    {
        //
    }
}
