<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class UserController extends Controller
{


    public function getQuotes(){

        $user = Auth::user();
        return view('user.my-quotes')->with('user', $user);

    }
    public function getAircraft(){
        
    }
    public function getSubscriptions(){
        
    }
    public function getProfile(){
        
    }
    public function dashboard(){
        
        return view('user.user-dashboard')->with('user', Auth::user());
        
    }

}


