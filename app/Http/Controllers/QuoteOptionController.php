<?php

namespace App\Http\Controllers;

use App\QuoteOption;
use App\Quote;
use Illuminate\Http\Request;
use App\Requirement;
use Auth;
use Illuminate\Support\Facades\Log;
class QuoteOptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( $requirementId, Request $request)
    {
        $requirement = Requirement::findOrFail($requirementId);

        $quote = $requirement->getQuoteByUser(Auth::user());

        if(! $quote){

            $quote = new Quote();
            $quote->requirement_id = $requirement->id;
            $quote->user_id = Auth::user()->id;
            $quote->status = 'open';
            $quote->confirmed = false;
            $quote->save();

        }

        $quoteOption = new QuoteOption();
        $quoteOption->setQuoteOptionId();
        $quoteOption->details = $request->input('details', '');
        $quoteOption->description = $request->input('description');
        $quoteOption->lead_time = $request->input('lead_time', null);
        $quoteOption->part_number = $request->input('part_number');
        $quoteOption->status = $request->input('status', 'open');
        $quoteOption->price = $request->input('price');
        $quoteOption->condition = $request->input('condition');
        $quoteOption->closed = $request->input('closed', false);
        $quoteOption->quote_id = $quote->id;
        $quoteOption->save();
        $quoteOption->load('quote');
        return $quoteOption;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\QuoteOption  $quoteOption
     * @return \Illuminate\Http\Response
     */
    public function show(QuoteOption $quoteOption)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\QuoteOption  $quoteOption
     * @return \Illuminate\Http\Response
     */
    public function edit(QuoteOption $quoteOption)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\QuoteOption  $quoteOption
     * @return \Illuminate\Http\Response
     */
    public function update(QuoteOption $quoteOption, Request $request )
    {
            $quoteOption->details = $request->input('details', '');
            $quoteOption->description = $request->input('description');
            $quoteOption->lead_time = $request->input('lead_time', null);
            $quoteOption->part_number = $request->input('part_number');
            $quoteOption->status = $request->input('status');
            $quoteOption->price = $request->input('price');
            $quoteOption->condition = $request->input('condition');
            $quoteOption->closed = $request->input('closed', false);
            $quoteOption->save();
            return $quoteOption;

    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\QuoteOption  $quoteOption
     * @return \Illuminate\Http\Response
     */
    public function destroy($optionId)
    {
        $quoteOption = QuoteOption::findOrFail($optionId);
        $quote = $quoteOption->quote;
        $quoteOption->delete();
        
        //if this quote option is the last one, let's delete the master quote.
        if($quote->quoteOptions->count() < 1 ){
            $quote->delete();    
        }

        return http_response_code(200);
    }

    /**
     * 
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Quote  $quote
     * @return \Illuminate\Http\Response
     */
    public function sync( Request $request )
    {
        $sessionKey = 'quotesStagedForResponse';
        $returnQuotes = collect();
        foreach( $request->all() as $inboundQuote ){
            $quoteOption = QuoteOption::find((int) $inboundQuote['id'])->load('quote');
            $requirement = $quoteOption->quote->requirement;
            $aircraft = $requirement->aircraft;
            $quoteOption->syncModel($inboundQuote);
            $quoteOption->requirement = $requirement;
            $quoteOption->requirement->aircraft = $aircraft;

            $returnQuotes->push($quoteOption);
        }
        $request->session()->put( $sessionKey, $returnQuotes);
        return $returnQuotes;
    }

    public function listByRequirement($requirementId)
    {
        $requirement = Requirement::findOrFail($requirementId);
        if($quote = $requirement->getQuoteByUser(Auth::user()))
        {
            return $quote->quoteOptions;
        }
            else {
                return collect();
            }
    }
}
