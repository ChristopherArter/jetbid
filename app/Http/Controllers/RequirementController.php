<?php

namespace App\Http\Controllers;

use App\Requirement;
use App\RequirementType;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Carbon\Carbon;
use App\Stats;

class RequirementController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( $requirements = null )
    {


                $requirements = Requirement::where('status', '=', 'open')->with('aircraft')->get();


            return view('requirements.index.index')->with('requirements', $requirements);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Requirement  $requirement
     * @return \Illuminate\Http\Response
     */
    public function show(Requirement $requirement)
    {

        $types = $requirement->types;
        $quotes = $requirement->quotes->where('user_id', '==', Auth::user()->id);
        $hasQuoted = Auth::user()->hasQuoted($requirement);

        return view('requirements.single')
            ->with('requirement', $requirement)
            ->with('aircraft')
            ->with('hasQuoted', $hasQuoted)
            ->with('types', $types)
            ->with('quotes', $quotes);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Requirement  $requirement
     * @return \Illuminate\Http\Response
     */
    public function edit(Requirement $requirement)
    {
        $user = Auth::user();
        // return $requirement;
       return view('requirements.create-edit')
       ->with( 'requirement', $requirement)
       ->with('aircrafts', $user->aircrafts)
       ->with('activeTypes', $requirement->types);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Requirement  $requirement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Requirement $requirement)
    {
        // if($request->requirementtypes){
        //     foreach($request->requirementtypes as $type){
        //         $requirement->requirement->type($type);
        //     }
        // }

        // $type = RequirementType::find(2);
        // $requirement->types()->save($type);
        $requirement->description = $request->description;
        $requirement->details = $request->details;
        $requirement->part_number = $request->part_number;
        $requirement->status = $request->status;
        $requirement->location = $request->location;
        $requirement->aircraft_id = $request->aircraft_id;
        $requirement->save();
        return $requirement;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Requirement  $requirement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Requirement $requirement)
    {
        //
    }

    public function search(){
        return view('requirements.search');
    }

    public function json(){
        $requirements = Requirement::where('status', '=', 'open')->with('aircraft')->get();
        return $requirements;
    }

    public function types( $id ) {
        $requirement = Requirement::find($id);
        return $requirement->types;
    }

    public function getOpen(){
        $requirements = Requirement::where('status', '=', 'open')->with('aircraft')->get();
        return view('requirements.index.index')->with('requirements', $requirements);
    }

    public function getRecent(){

        $requirements = Requirement::where('status', '=', 'open')->
        where('created_at', '==', Carbon::now()->subDays(7))
        ->with('aircraft')->get();

        return view('requirements.index.index')->with('requirements', $requirements);
        
    }

    public function getRecievedQuotes( Request $request){

        $user = Auth::user();
        $quotes = collect();
        $userRequirements = $user->requirements;
        foreach( $userRequirements as $requirement){
            foreach( $requirement->quotes as $quote ){
                foreach( $quote->quoteOptions as $quoteOption)
                {
                    $quoteOption->quote->load('requirement');
                    $quoteOption->quote->requirement->aircraft = $quote->aircraft();
                    $quotes->push($quoteOption);
                }
            }
        }

        

        return view('requirements.recieved-quotes')->with('quotes', $quotes);
    }

    public function getJson( Requirement $requirement )
    {
        $requirement->quotes = $requirement->getQuoteByUser(Auth::user());
        return $requirement;
    }
}
