<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Quote;
use Auth;
use Carbon\Carbon;
use App\Requirement;

class QuoteOption extends Model
{
    
    protected $table = 'quote_options';

    public function quote()
    {
        return $this->belongsTo('App\Quote');
    }

    public function setQuoteOptionId()
    {
        try {
            $this->quote_option_id = config('quotes.quote_option_prefix') . strtoupper(str_random(6));
          }
          catch (\Exception $e) {
            $this->quote_option_id = config('quotes.quote_option_prefix') . strtoupper(str_random(6));
          }  
    }

       /**
    * Takes array and maps it to an existing model.
    *
    * @param array $quote
    * @return void
    */
   public function syncModel(  $quote ){

    $user = Auth::user();

    if( isset( $quote['created_at' ] ) ) {

        // TO DO: Make this a middleware
       if( $this->user_id == $user->id ) {

        $this->updated_at = Carbon::now()->toDateTimeString();
        $this->description = $quote['description'];
        $this->lead_time = $quote['lead_time'];
        $this->user_id = Auth::user()->id;
        $this->user = Auth::user();
        $this->status = $quote['status'];
        $this->price = (int) $quote['price'];
        $this->requirement_type_id = $quote['requirement_type_id'];
        $this->condition_id = (int) $quote['condition_id'];

        return $this;
       }
    }
   }
    
}
